package com.boya.demo.config;

import com.baidu.fis.servlet.MapListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * fis3-jello 配置
 */
@Configuration
//@ConfigurationProperties(prefix = "spring.velocity")
public class Fis3JelloConfig extends WebMvcConfigurerAdapter {

    @Bean
    public MapListener getMapListener() {
        return new MapListener();
    }

/*
    private Boolean cache = false;

    @Bean(name = "fisInit")
    public FisBean getFisBean() {
        return new FisBean();
    }


    @Bean(name = "velocityViewResolver")
    public VelocityViewResolver getViewResolver() {
        VelocityViewResolver viewResolver = new VelocityViewResolver();
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".vm");
        viewResolver.setContentType("text/html;charset=UTF-8");
        viewResolver.setRequestContextAttribute("request");
        viewResolver.setCache(cache);
        viewResolver.setCacheUnresolved(false);
        viewResolver.setExposeContextBeansAsAttributes(true);
        viewResolver.setExposePathVariables(true);
        viewResolver.setExposeRequestAttributes(true);
        viewResolver.setExposeSpringMacroHelpers(true);
        viewResolver.setExposeSessionAttributes(true);
        Map<String, Object> attributesMap = new HashMap<String, Object>();
        attributesMap.put("esc", new EscapeTool());
        attributesMap.put("render", new RenderTool());
        attributesMap.put("link", new LinkTool());
        attributesMap.put("context", new ContextTool());
        attributesMap.put("dateTool", new DateTool());
        attributesMap.put("params", new ParameterTool());
        attributesMap.put("jello", new JelloTool());
        viewResolver.setAttributesMap(attributesMap);
        return viewResolver;
    }

    @Bean
    public VelocityConfigurer velocityConfigurer() {
        VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
        velocityConfigurer.setResourceLoaderPath("/WEB-INF/views/");

        Properties velocityProperties = new Properties();
        velocityProperties.setProperty("input.encoding", "UTF-8");
        velocityProperties.setProperty("output.encoding", "UTF-8");
        velocityProperties.setProperty("userdirective",
                "com.baidu.fis.velocity.directive.Html,"
                        + "com.baidu.fis.velocity.directive.Head,"
                        + "com.baidu.fis.velocity.directive.Body,"
                        + "com.baidu.fis.velocity.directive.Require,"
                        + "com.baidu.fis.velocity.directive.Script,"
                        + "com.baidu.fis.velocity.directive.Style,"
                        + "com.baidu.fis.velocity.directive.Uri,"
                        + "com.baidu.fis.velocity.directive.Widget,"
                        + "com.baidu.fis.velocity.directive.Block,"
                        + "com.baidu.fis.velocity.directive.Extends");

        velocityConfigurer.setVelocityProperties(velocityProperties);

        return velocityConfigurer;
    }
*/
}
